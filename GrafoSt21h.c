/* Librerías en órden alfabético */
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
/* Archivos útiles del proyecto */

#include "GrafoSt21h.h"
#include "RomaVictor.h"
#include "get_v_w.h"


typedef struct vecinos_{
    u32 numero;
    u32 peso; 
}vecinos_;  

typedef struct laMagia_t{
    u32 grado;
    u32 nombre;
    u32 color;
    vecinos_ **vecinos; //   ((grafo->vertices[numero]).vecinos[numero])->numero;    

}laMagia_t;
typedef struct GrafoSt{
    u32 delta;
    u32 cantidad_vertices;
    u32 cantidad_lados;
    u32 *orden;
    struct laMagia_t *vertices;
}GrafoSt;

/* ERROR_ : no se encontro el vertice o el vecino
 * causa:la posicion el vertice o el vecino sea mas grande
 * al numero de vertice que tenga el grafo y/o mas grande que 
 * el numero de vecinos que tenga el vertice i
 * 42949667295=2^32-1 */
#define ERROR_ 4294967295

grafo destruccionDelGrafo(grafo grafo){
    u32 grado;
    for(u32 i = 0u; i < grafo->cantidad_vertices; ++i){        
        grado = grafo->vertices[i].grado;
        /*Liberar varias cosas antes*/
        for(u32 j = 0u; j < grado; ++j){
            free(grafo->vertices[i].vecinos[j]);
            grafo->vertices[i].vecinos[j] = NULL;
        }

        free( (grafo->vertices[i]).vecinos );
        (grafo->vertices[i]).vecinos = NULL;
    }
        
    free( grafo->vertices );
    grafo->vertices = NULL;
    free( grafo->orden );
    grafo->orden = NULL;

    free(grafo);
    grafo = NULL;    
    return grafo;
}

grafo construccionDelGrafo(){

    /* Declaraciones y asignaciones para input */    
    u32 *v_w = v_and_w();
    char cadena[100]; char *token = NULL; 
    u32 a; u32 b; u32 a_mod; u32 b_mod;

    /* Declaraciones y asignaciones algoritmos */
    u32 primero = 0u; 
    u32 asig_num = 1u; 
    u32 asig_delta = 0u; //Dará delta
    u32 *a_d_n = calloc(MAX_SIZE, sizeof(u32));// = {0u};

    /* MEM: Declaraciones, asignaciones y reservas */ 
    grafo nuevo_grafo = (grafo)malloc( sizeof(struct GrafoSt) );
    assert(nuevo_grafo!=NULL);     
    nuevo_grafo->vertices = (laMagia_t*)calloc( v_w[0], sizeof(laMagia_t));
    assert(nuevo_grafo->vertices!=NULL);
    nuevo_grafo->orden = (u32*)calloc(v_w[0], sizeof(u32)); 
    assert(nuevo_grafo->orden!=NULL);    
    for(u32 ii = 0u; ii < v_w[0]; ++ii){
        nuevo_grafo->vertices[ii].grado = 0u;
    }

    /* Declaraciones para acortar codigo */
    vecinos_ **reserva_a = NULL;        
    u32 grado;

    /* Completamos parcialmente datos de grafo */ 
    nuevo_grafo->cantidad_vertices = v_w[0];
    nuevo_grafo->cantidad_lados = v_w[1];


    for ( u32 i = 0u ; i < v_w[1] ; ++i ){
        if(fgets(cadena, 100, stdin) == NULL){
            assert(cadena != NULL);
        }
        token = strtok(cadena, "e");
        sscanf( token, "%u%u", &a,&b);
        a_mod = a % MAX_SIZE;
        b_mod = b % MAX_SIZE;

        if(i == 0u){

            /*Si iría*/
            primero = a_mod;
            nuevo_grafo->vertices[0u].nombre = a;
            ( (nuevo_grafo->vertices[0u] ).vecinos ) = (vecinos_**)malloc(sizeof(vecinos_*));
            assert(nuevo_grafo->vertices[0u].vecinos != NULL); 
            /**/   
        
        } 
        if( a != primero && a_d_n[a_mod] == 0u ){
            
            /*Si iría*/
            a_d_n[a_mod] = asig_num;
            nuevo_grafo->vertices[a_d_n[a_mod]].nombre = a;
            ((nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos) = (vecinos_**)malloc(sizeof(vecinos_*));
            assert( (nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos != NULL );
            ++asig_num;
            /**/           
        } 
        if( b != primero && a_d_n[b_mod] == 0u ){            
            /*Si iría*/
            a_d_n[b_mod] = asig_num;
            nuevo_grafo->vertices[a_d_n[b_mod]].nombre = b;
            ((nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos) = (vecinos_**)malloc(sizeof(vecinos_*));
            assert( (nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos != NULL );
            ++asig_num;
            /**/
        }

        /* Actualizo datos de A respecto de B */

        /*Si iría*/
        ++((nuevo_grafo->vertices[a_d_n[a_mod]]).grado);
        grado = nuevo_grafo->vertices[a_d_n[a_mod]].grado;

        //if( grado-1 % 1500 != 1 ){
        reserva_a = (vecinos_**)realloc( nuevo_grafo->vertices[a_d_n[a_mod]].vecinos,
                                         (grado) * sizeof(vecinos_*)              );
        assert( reserva_a != NULL );
        (nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos = reserva_a;
        //}
        //reserva_a = (vecinos_**)realloc( nuevo_grafo->vertices[a_d_n[a_mod]].vecinos,
        //                                 grado * sizeof(vecinos_*)              );
        //assert( reserva_a != NULL );
        //(nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos = reserva_a;
        (nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos[( grado - 1 )] = calloc(1, sizeof(vecinos_));
        assert( (nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos[( grado - 1 )] != NULL  );
        (nuevo_grafo->vertices[a_d_n[a_mod]]).vecinos[( grado - 1 )]->numero = a_d_n[b_mod];
        /**/

        /* Actualizo datos de B respecto de A */

        /*Si iría*/
        ++((nuevo_grafo->vertices[a_d_n[b_mod]]).grado);
        grado = nuevo_grafo->vertices[a_d_n[b_mod]].grado;
        
        //if( grado-1 % 1500 != 1){
        reserva_a = (vecinos_**)realloc( nuevo_grafo->vertices[a_d_n[b_mod]].vecinos,
                                         (grado) * sizeof(vecinos_*)             );
        assert( reserva_a != NULL );
        (nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos = reserva_a;
        //}

        (nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos[( grado - 1 )] = calloc(1, sizeof(vecinos_));
        assert( (nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos[( grado - 1 )] != NULL ); 
        (nuevo_grafo->vertices[a_d_n[b_mod]]).vecinos[( grado - 1 )]->numero = a_d_n[a_mod];
        /**/

        
        asig_delta = fmax( asig_delta,
                                       fmax( (nuevo_grafo->vertices[a_d_n[a_mod]]).grado,
                                             (nuevo_grafo->vertices[a_d_n[b_mod]]).grado
                                           ) 
                         );
                         
    }
    nuevo_grafo->delta = asig_delta; 
    free(v_w);
    v_w = NULL;
   
    return nuevo_grafo;
}

grafo copiarGrafo(grafo G){
    grafo grafo_copia = calloc(1,sizeof(struct GrafoSt));
    assert(grafo_copia!=NULL);
    /* Para acortar lineas de código */    
    u32 g_d_v;  vecinos_ **copia_adv=NULL;
    /*Se copian datos generales del grafo*/
    grafo_copia->cantidad_vertices = G->cantidad_vertices;
    grafo_copia->cantidad_lados = G->cantidad_lados;
    grafo_copia->delta = G->delta;
   
    
    grafo_copia->vertices = (struct laMagia_t*)calloc(G->cantidad_vertices, 
                                     sizeof(struct laMagia_t));
    assert(grafo_copia->vertices != NULL);                                  

    //grafo_copia->vertices->vecinos = (vecinos_ **) malloc ( sizeof(vecinos_ *) );

    /* Copia datos por vértices específico */    
    for(u32 i = 0u; i < G->cantidad_vertices; ++i){
        /* ASIGNACIONES PARA ACORTAR LINEAS DE CÓDIGO */
        g_d_v = (G->vertices[i]).grado; /* Número de vecinos  */
        //a_d_v = (G->vertices[i]).vecinos;     /* Arreglos de vecinos*/ 
        //copia_adv = (grafo_copia->vertices[i]).vecinos;
        
        //copia_adv = (vecinos_ **)malloc(sizeof(vecinos_*)); 
        copia_adv = (vecinos_ **)calloc(g_d_v, sizeof(vecinos_*)); //Revisar esta linea
        assert(copia_adv != NULL);
        grafo_copia->vertices[i].vecinos = copia_adv;        

        /* Se copia número y peso de cada vecino */
        for(u32 j = 0u; j < g_d_v; ++j){
            copia_adv[j] = calloc(1,sizeof(vecinos_));
            assert(copia_adv[j]!=NULL);
            copia_adv[j]->numero = G->vertices[i].vecinos[j]->numero;
            copia_adv[j]->peso = G->vertices[i].vecinos[j]->peso;            
        }
        (grafo_copia->vertices[i]).grado = g_d_v;        
        (grafo_copia->vertices[i]).color = (G->vertices[i]).color;
        (grafo_copia->vertices[i]).nombre = (G->vertices[i]).nombre;
        grafo_copia->vertices[i].vecinos = copia_adv;
        copia_adv = NULL;
    }
    /* Se copia el orden "Natural" */
    grafo_copia->orden = calloc(G->cantidad_vertices,
                                        sizeof(u32)); 
    for(u32 i = 0u; i < G->cantidad_vertices; ++i){
        grafo_copia->orden[i] = G->orden[i];
    }                                    
    /*Alteramos punteros colgantes*/

    return grafo_copia;
}


 
u32 numeroDeVertices(grafo G){
    return G->cantidad_vertices;
}

u32 numeroDeLados(grafo G){
    return G->cantidad_lados;
}

u32 deltah(grafo G){
    return G->delta;
}

u32 nombre(u32 i, grafo G){
    if(G->cantidad_vertices < i){
        //i es demasiado grande
        return ERROR_;
    }else{
    return (G->vertices[G->orden[i]]).nombre;
    }
}

u32 color(u32 i, grafo G){
   if(G->cantidad_vertices < i){
        //i es demasiado grande
        return ERROR_;
    }else{
    return (G->vertices[G->orden[i]]).color;
    }
}

u32 grado(u32 i, grafo G){    
    if(G->cantidad_vertices < i){
        //i es demasiado grande
        return ERROR_;
    }else{
    return (G->vertices[G->orden[i]]).grado;
    }
}

u32 ColorVecino(u32 j,u32 i,grafo G){
    assert(G!=NULL && (G->cantidad_vertices > i));
    u32 j_natural = G->vertices[i].vecinos[j]->numero;
    return G->vertices[(G->orden[j_natural])].color;
}

u32 NombreVecino(u32 j,u32 i,grafo G){
    /* acorto código a vertice i */   
    assert(G!=NULL && (G->cantidad_vertices > i));
    u32 j_natural = G->vertices[i].vecinos[j]->numero;
    return G->vertices[(G->orden[j_natural])].nombre;
}

u32 OrdenVecino(u32 j, u32 i, grafo G){
    assert(G!=NULL && G->cantidad_vertices > i);
    u32 j_natural = G->vertices[i].vecinos[j]->numero;  
    return G->vertices[(G->orden[j_natural])].grado;
}

u32 PesoLadoConVecino(u32 j, u32 i, grafo G){
    assert(G!=NULL);
    return G->vertices[i].vecinos[j]->peso;
}

char FijarColor(u32 x, u32 i, grafo G){
    assert(G!=NULL);
    if(i < G->cantidad_vertices){
        (G->vertices[G->orden[i]]).color = x;
        return 0;        
    }
    return 1;
}

char FijarOrden(u32 i,grafo G,u32 N) {
    assert(G!=NULL);    
    u32 tam_vert = G->cantidad_vertices;
    if (i < tam_vert && N < tam_vert ) {
    G->orden[N] = i;     
        return 0u;
    }
    return 1u;
}

u32 FijarPesoLadoConVecino(u32 j, u32 i, u32 p, grafo G){
    assert(G!=NULL);
    u32 tam_vert = G->cantidad_vertices;
    if (i < tam_vert && j < tam_vert ) {
        (G->vertices[i]).vecinos[j]->peso = p;
        return 0u;
    }
    return 1u;
}

