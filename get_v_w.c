#include<assert.h>
#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include<stdlib.h>
#include"RomaVictor.h" 
#include"get_v_w.h"
#include"GrafoSt21h.h" 

u32* v_and_w(){
    bool inicio = true;    char cadena[100];  
    u32 *retorno = (u32*) malloc (2 * sizeof(u32));

	while(inicio){
		if(fgets(cadena,100,stdin) != NULL){
    		if(cadena[0]=='p'){ 
			inicio = false;
		    }	            
        }		
	}        

    if( strstr(cadena," edge ") == NULL ){
        free(retorno);
        fprintf(stderr, "Datos inválidos.\n");
        exit(EXIT_FAILURE); 
    }

    char *token = strtok(cadena, "e");
    token = strtok(NULL, "e"); 
    token = strtok(NULL, "e"); 

    sscanf( token, "%u%u", &retorno[0], &retorno[1]);
    
    cadena[strlen(cadena) - 1 ] = '\0';   

    return retorno;
}