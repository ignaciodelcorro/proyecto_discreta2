CC = gcc
CFLAGS = -Wall -Werror -Wextra -O -g -pedantic -std=c99 

TARGET = main
TARGET2 = GrafoSt21h
TARGET3 = get_v_w

all: main.c GrafoSt21h.c get_v_w.c
		$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c $(TARGET2).c $(TARGET3).c -lm

clean:
		$(RM) $(TARGET) $(TARGET2) $(TARGET3)
