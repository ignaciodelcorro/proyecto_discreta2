#ifndef GRAFO_H
#define GRAFO_H
#include "GrafoSt21h.h"

typedef struct GrafoSt* grafo;


/* grafo_t es un puntero a una estructura.
 * Nota: La definicion de la estructura
 * podría estar en [grafo.c], no acá */  

grafo construccionDelGrafo();
/* Crea un grafo 
 * 
 * PRE: {true}
 * graph = construccionDelGrafo();
 * POS: {graph --> graph_t /\ delta(graph) > 0}
*/

grafo destruccionDelGrafo(grafo G);
/*
 *Destruye el grafo
 * 
*/ 

grafo copiarGrafo(grafo G);
/*
 *nuevoGrafo = copiarGrafo(grafo);
 *Copy grafo to nuevoGra
*/   

// FUNCIONES CON ORDEN 0(1)
u32 numeroDeVertices(grafo G);
/*Devuelve el número de vertices 
 *
 * 
*/
u32 numeroDeLados(grafo G);
/*Devuelve el número de lados
 *
 * 
*/
u32 deltah(grafo G);
/*
 * Devuelve el mayor grado del grafo. 
 *  
*/
u32 nombre(u32 i, grafo G);
/* Devuelve el nombre real del vértice 
 * número i en el orden guardado en ese
 * momento en G.
*/
u32 color(u32 i, grafo G);
/* Devuelve el color del vértice i 
 * en el orden actual correspondiente   
 * 
*/
u32 grado(u32 i, grafo G);
/*
 * Devuelve el grado del vértice i
 * en el orden actual correspondiente 
*/
u32 ColorVecino(u32 j, u32 i, grafo G);
/*
 * Devuelve el color del vecino número j 
 * del vértice i en el orden guardado
 * en ese momento.  
 * 
*/

u32 NombreVecino(u32 j, u32 i, grafo G);
/*
 * Devuelve el nombre del vécino número j
 * del vértice i en el orden guardado
 * en ese momento.
 * 
*/

u32 OrdenVecino(u32 j, u32 i, grafo G);
/*
 * Devuelve el orden del vecino j
 * del vértice i en el orden guardado
 * en ese momento en grafo
*/   

u32 PesoLadoConVecino(u32 j, u32 i, grafo G);
/*
 * Devuelve el peso del lado formado por.
 * el i-ésimo vértice de G en el orden interno 
 * con el j-ésimo vecino de ese vértice
*/
char FijarColor(u32 x, u32 i, grafo G);
/*
 * Si i es menor que el número de vértices, le 
 * asigna el color x al vértice número i en el 
 * orden guardado en ese momento en
 * grafo y retorna 0.De lo contrario, retorna 1.
*/

char FijarOrden(u32 i, grafo G, u32 N);
/*
 * Si i y N son menores que el número de vértices,
 * fija que el vértice i en el orden guardado en 
 * G será el N -ésimo vértice
 * del orden “natural”(el que se obtiene al ordenar 
 * los vértices en orden creciente de sus “nombres” reales) y 
 * retorna 0. De lo contrario retorna 1.
*/
u32 FijarPesoLadoConVecino(u32 j, u32 i, u32 p, grafo G);
/*
 * Hace que el lado formado por el i-ésimo vértice de G 
 * en el orden interno con el j-ésimo vecino de ese vértice tenga peso p
*/

#endif