#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "RomaVictor.h"

int main(){
    
    /* Creación del grafo */
    char error;
    grafo G = construccionDelGrafo();
    for(u32 i = 0u; i<numeroDeVertices(G); i++){
        error = FijarOrden(i,G,i);
        printf("%c",error);
    }
    if(-1 == FijarOrden(0,G,1)){
        exit(-1);
    }

    printf("Número de vértices: %d\n Número de lados: %d\n Delta: %d\n",
            numeroDeVertices(G), numeroDeLados(G), deltah(G));
    /*        
    for(u32 i = 0u; i< numeroDeVertices(G); ++i){
        printf("Datos de vértice número %i: \n", i+1);
        printf("   Nombre: %u   \n   Grado: %u.  \n   Color: %u\n",
                    nombre(i, G), grado(i, G), color(i,G));
    }*/
    

    grafo G2 = copiarGrafo(G);


    printf("Número de vértices: %d\n Número de lados: %d\n Delta: %d\n",
            numeroDeVertices(G2), numeroDeLados(G2), deltah(G2));
 /*           
    for(u32 i = 0u; i< numeroDeVertices(G2); ++i){
        printf("Datos de vértice número %i: \n", i+1);
        printf("   Nombre: %u   \n   Grado: %u.  \n   Color: %u\n",
                    nombre(i, G2), grado(i, G2), color(i,G2));
    }*/
    G2 = destruccionDelGrafo(G2);
    
    G = destruccionDelGrafo(G);
    return 0;
}