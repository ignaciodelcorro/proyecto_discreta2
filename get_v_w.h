#ifndef GET_V_W_H
#define GET_V_W_H
#include "GrafoSt21h.h"

/* Return array: size(array) = 2
 * array[0] == cantidad_vertices(grafo)    
 * array[1] == cantidad_lados(grafo) */
u32* v_and_w();

#endif